var app = new Vue({
  el: "#mycontent",
  data:{
    demo_data: [
      {
        src_audio_url: "/static/audios/LSTM/en0351_natural.wav",
        tgt_natural_audio_url: "/static/audios/LSTM/ja0351_natural.wav",
        tgt_no_em_audio_url: "/static/audios/LSTM/ja0351_noem.wav",
        tgt_em_no_pause_audio_url: "/static/audios/LSTM/ja0351_LSTM.wav",
        tgt_em_pause_audio_url: "/static/audios/LSTM/ja0351_LSTM_pause.wav",
        text_en: "It sometimes hurt",
        text_ja: "時々痛みます",
        text_pause: "時々、痛みます",
        emph_en: "0.1 0.8 0.2",
        emph_ja: "0.7 0.2 0.1",
        title: "demo1",
        duration: 1000,
        id: 0
      },
      {
        src_audio_url: "/static/audios/crf_pause/original-English.wav",
        tgt_natural_audio_url: "/static/audios/crf_pause/original-Japanese.wav",
        tgt_no_em_audio_url: "",
        tgt_em_no_pause_audio_url: "/static/audios/crf_pause/trans_emp_wo_pause.wav",
        tgt_em_pause_audio_url: "/static/audios/crf_pause/trans_emp_w_pause.wav",
        text_en: "Coffee or tea",
        text_ja: "コーヒーそれとも紅茶になさいますか",
        text_pause: "コーヒー、それとも、紅茶、になさいますか",
        emph_en: "0.8 0.1 0.7",
        emph_ja: "0.7 0.2 0.6 0.1 0.1 0.0 0.1",
        title: "demo2",
        duration: 1000,
        id: 1
      },
    ],
    selected: 0,
    asr_ready: false,
    pause_ready: false,
    mt_ready: false,
    tts_ready: false,
    es_ready: false,
    running_asr: false,
    is_start_demo: false,
    running_mt: false,
    running_pause: false,
    running_tts: false,
    running_es: false,
    item: {
      src_audio_url: "/static/audio/1.wav",
      text_en: "Coffee or tea",
      text_ja: "コーヒー",
      text_pause: "コーヒー、",
      emph_en: "0.8 0.1 0.7",
      emph_ja: "0.7 0.1 0.1 0.1"
    }
  },
  computed: {
  },
  mounted(){
  },
  methods: {
    start_asr: function(){
      _this = this;
      _this.running_asr = true;
      setTimeout(function(){
        _this.asr_ready = true;
        _this.running_asr = false;
        _this.start_es();
      }, _this.item.duration);
    },
    start_es: function(){
      _this = this;
      _this.running_es = true;
      setTimeout(function(){
        _this.es_ready = true;
        _this.running_es = false;
        _this.start_mt();
      }, 1000);
    },
    start_mt: function(){
      _this = this;
      _this.running_mt = true;
      setTimeout(function(){
        _this.mt_ready = true;
        _this.running_mt = false;
        _this.start_pause();
      }, 1000);
    },
    start_tts: function(){
      _this = this;
      _this.running_tts = true;
      setTimeout(function(){
        _this.tts_ready = true;
        _this.running_tts = false;
      }, 1000);
    },
    start_pause: function(){
      _this = this;
      _this.running_pause = true;
      setTimeout(function(){
        _this.pause_ready = true;
        _this.running_pause = false;
        _this.start_tts();
      }, 1000);
    },
    start_demo: function(){
      this.on_select_change();
      this.item = this.demo_data[this.selected];
      this.is_start_demo = true;
      this.start_asr();
    },
    on_select_change: function(){
      this.running_asr = false;
      this.running_mt = false;
      this.running_tts = false;
      this.running_es = false;
      this.asr_ready = false;
      this.es_ready = false;
      this.mt_ready = false;
      this.tts_ready = false;
      this.is_start_demo = false;
      console.log("on select changed");
    }
  }
});
